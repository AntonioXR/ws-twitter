var database = require("./database"),
comentario = {}

comentario.selectAll = (idPublicacion, callback)=>{
    database.query("SELECT co.*, us.nombre AS nombreUsuario, DATE_FORMAT(co.fechaCreacion, '%Y-%m-%d') AS fechaFormat FROM comentarios co INNER JOIN usuario us ON us.idUsuario = co.idUsuario WHERE idPublicacion = ?;", idPublicacion || 0, (err, respose)=>{
        if(respose != null){
            callback(null, respose)
        }else{
            callback(null,{Mensaje: "Datos no encontrados"})
            throw err;
        }
    })
}

comentario.totalC = (idPublicacion, callback)=>{
    database.query("SELECT COUNT(*) AS totalComentaios FROM comentarios WHERE idPublicacion = ?;", idPublicacion || 0, (err, respose)=>{
        if(respose != null){
            callback(null, respose)
        }else{
            callback(null,{Mensaje: "Datos no encontrados"})
            throw err;
        }
    })
}

comentario.insert = (data, callback)=>{
    database.query("INSERT INTO comentarios(comentario, idPublicacion, idUsuario, fechaCreacion) VALUES (?,?,?,NOW());", data, (err, response)=>{
        if(response != null){
            callback(null, {Mensaje: true})
        }else{
            callback(null, {Mensaje: false})
        }
    })
}

comentario.delete = (idComentario, callback)=>{
    console.log(idComentario)
    database.query("DELETE FROM comentarios WHERE idComentario = ?;",idComentario, (err, response)=>{
        if(response != null){
            callback(null, {Mensaje: true})
        }else{
            callback(null, {Mensaje: false})
        }
    })
}

module.exports = comentario