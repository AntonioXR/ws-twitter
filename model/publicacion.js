var database = require("./database"),
publicacion  = {}

publicacion.selectAll = (callback)=>{
    database.query("SELECT pu.*, us.nombre AS nombreUsuario ,DATE_FORMAT(pu.fechaCreacion,'%Y-%m-%d') AS fechaFormat FROM publicacion pu INNER JOIN usuario us ON pu.idUsuario = us.idUsuario ORDER BY fechaCreacion DESC;", (err, response)=>{
        if(response != null){
            callback(null,response)
        }
        else{
            callback(null,{Mensaje: "Datos no encontrados"})
            throw err;
        }
    })
}

publicacion.selectMy = (idUsuario, callback)=>{
    database.query("SELECT pu.*, us.nombre AS nombreUsuario ,DATE_FORMAT(pu.fechaCreacion,'%Y-%m-%d') AS fechaFormat FROM publicacion pu INNER JOIN usuario us ON pu.idUsuario = us.idUsuario WHERE pu.idUsuario = ? ORDER BY fechaCreacion DESC;", idUsuario, (err, response)=>{
        if(response != null){
            callback(null,response)
        }
        
        else{
            callback(null,{Mensaje: "Datos no encontrados"})
            throw err;
        }
    })
}

publicacion.select = (idPublicacion, callback)=>{
    database.query("SELECT pu.*, us.nombre AS nombreUsuario ,DATE_FORMAT(pu.fechaCreacion,'%Y-%m-%d') AS fechaFormat FROM publicacion pu INNER JOIN usuario us ON pu.idUsuario = us.idUsuario WHERE pu.idPublicacion= ? ORDER BY fechaCreacion DESC;", idPublicacion, (err, response)=>{
        if(response != null){
            callback(null,response)
        }
        else{
            callback(null,{Mensaje: "Datos no encontrados"})
            throw err;
        }
    })
}

publicacion.insert = (data, callback)=>{
    database.query("INSERT INTO publicacion(contenido, imagen, idUsuario, fechaCreacion) VALUES (?, ?, ?, NOW());",data, (err, response)=>{
        if(response != null){
            callback(null,{Mensaje: true})
        }
        else{
            callback(null,{Mensaje:false})
            throw err;
        }
    })
}


publicacion.update = (data, callback)=>{
    database.query("UPDATE publicacion SET contenido = ?, imagen = ? WHERE idPublicacion = ? ;",data, (err, response)=>{
        if(response != null){
            callback(null,{Mensaje: true})
        }
        else{
            callback(null,{Mensaje:false})
            throw err;
        }
    })
}

publicacion.delete = (idPublicacion, callback)=>{
    database.query("CALL sp_eliminarPub(?);",idPublicacion, (err, response)=>{
        if(response != null){
            callback(null,{Mensaje: true})
        }
        else{
            callback(null,{Mensaje:false})
            throw err;
        }
    })
}

module.exports = publicacion