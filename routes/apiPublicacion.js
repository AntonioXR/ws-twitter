var publicacion = require("../model/publicacion"),
router = require("express").Router()

router.route("/api/publicacion/:idPublicacion")
    .put((req, res, next)=>{
        if(req.params.idPublicacion == req.body.idPublicacion)
        publicacion.update([req.body.contenido, req.body.imagen, req.body.idPublicacion],
        (err, response)=>{
            res.json(response)
        })
    })
    .delete((req, res, next)=>{
        publicacion.delete(req.params.idPublicacion,(err, response)=>{
            res.json(response)
        })
    })
    .get((req, res, next)=>{
        publicacion.select(req.params.idPublicacion, (err, response)=>{
            res.json(response)
        })
    })

router.route("/api/publicacion/")
    .post((req, res)=>{
            publicacion.insert([req.body.contenido,req.body.imagen, req.body.idUsuario],
        (err, response)=>{
            res.json(response)
        })
    })
    .get((req,res)=>{
        publicacion.selectAll((err, response)=>{
            res.json(response)
        })
    })

router.get("/api/publicacion/yo/:idUsuario", (req,res)=>{
    publicacion.selectMy(req.params.idUsuario, (err, response)=>{
        res.json(response)
    })
})

module.exports = router;