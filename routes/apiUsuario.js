var usuario = require("../model/usuario"),
router = require("express").Router()

router.route("/api/usuario/:idUsuario")
    .put((req, res, next)=>{
        if(req.params.idUsuario == req.body.idUsuario){
            usuario.update([req.body.nombre,req.body.nick,req.body.contrasena , req.body.imagen, req.body.idUsuario], (err, respose)=>{
                    res.json(respose)
            })
        }else{
            res.json({Mensaje: "No concuerdan los datos"})
        }
    })
    .delete((req, res, next)=>{
        usuario.delete(req.params.idUsuario, (err, respose)=>{
                res.json(respose)
        })
    })
router.post("/api/usuario", (req, res, next)=>{
    usuario.insert([req.body.nombre,req.body.nick,req.body.contrasena], (err, respose)=>{
        res.json(respose);
    })
})
router.post("/api/usuario/autenticar", (req, res, next)=>{
    var data = [req.body.nick,req.body.contrasena]
    usuario.autenticar(data, (err, respose)=>{
        if(respose != null){
            var respuesta = respose[0]
            respuesta.state = true;
            respuesta.Mensaje = "usuario valido"
            res.json(respuesta)
        }else{
            var respuesta = {
                Mensaje: "Usuario no encontrado",
                state: false
            }
            res.json(respuesta)
        }
    })
})

module.exports = router;