var comentario = require("../model/comentario"),
router = require("express").Router()

router.get("/api/comentario/:idPublicacion", (req, res)=>{
    comentario.selectAll(req.params.idPublicacion, (err, response)=>{
        res.json(response)
    })
})

router.get("/api/comentario/total/:idPublicacion", (req, res)=>{
    comentario.totalC(req.params.idPublicacion, (err, response)=>{
        res.json(response)
    })
})

router.post("/api/comentario/", (req, res)=>{
    comentario.insert([req.body.comentario, req.body.idPublicacion, req.body.idUsuario], (err, response)=>{
        res.json(response)
    })
})

router.delete("/api/comentario/:idComentario", (req, res)=>{
    comentario.delete(req.params.idComentario, (err, respose)=>{
        res.json(respose)
    })
})

module.exports= router;