CREATE DATABASE twitterIonic;
USE twitterIonic;

CREATE TABLE usuario(
	idUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(60) NOT NULL,
	nick VARCHAR(20) NOT NULL,
	contrasena VARCHAR(20) NOT NULL,
	fechaCreacion DATETIME
);

CREATE TABLE publicacion(
	idPublicacion INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	contenido TEXT,
	imagen VARCHAR(100),
	idUsuario INT,
	fechaCreacion DATETIME,
	FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

CREATE TABLE comentarios (
	idComentario INT AUTO_INCREMENT NOT NULL,
	comentario TEXT,
	idPublicacion INT,
	idUsuario INT,
	fechaCreacion DATETIME,
	PRIMARY KEY (idComentario),
	FOREIGN KEY (idPublicacion) REFERENCES publicacion (idPublicacion),
	FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario)
);

DELIMITER $$
CREATE PROCEDURE sp_eliminarUsuario( IN _idUsuario INT)
BEGIN 
	DELETE FROM comentario WHERE idPublicacion IN (SELECT idPublicacion FROM publicacion WHERE idUsuario = _idUsuario);
	DELETE FROM publicacion WHERE idUsuario = _idUsuario;
	DELETE FROM usuario WHERE idUsuario = _idUsuario;
	
END $$