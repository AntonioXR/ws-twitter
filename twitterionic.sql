-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-08-2017 a las 18:59:24
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `twitterionic`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminarPub` (IN `_idPublicacion` INT)  BEGIN 
		DELETE FROM comentarios WHERE idPublicacion = _idPublicacion; 
		DELETE FROM publicacion WHERE idPublicacion = _idPublicacion; 
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminarUsuario` (IN `_idUsuario` INT)  BEGIN 
	DELETE FROM comentario WHERE idPublicacion IN (SELECT idPublicacion FROM publicacion WHERE idUsuario = _idUsuario);
	DELETE FROM publicacion WHERE idUsuario = _idUsuario;
	DELETE FROM usuario WHERE idUsuario = _idUsuario;
	
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idComentario` int(11) NOT NULL,
  `comentario` text,
  `idPublicacion` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idComentario`, `comentario`, `idPublicacion`, `idUsuario`, `fechaCreacion`) VALUES
(3, '123123123', 2, 1, '2017-08-03 09:44:42'),
(5, '4356345634563456', 2, 2, '2017-08-03 09:46:14'),
(6, '987654987', 6, 2, '2017-08-03 09:46:52'),
(7, '12312313 ', 6, 1, '2017-08-03 09:49:05'),
(8, 'Khe rika nena', 2, 1, '2017-08-03 10:11:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE `publicacion` (
  `idPublicacion` int(11) NOT NULL,
  `contenido` text,
  `imagen` varchar(100) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`idPublicacion`, `contenido`, `imagen`, `idUsuario`, `fechaCreacion`) VALUES
(2, 'Esta es una publicacion con imagen Jajaj Khe', 'http://localhost:3000/images/1a53e6042bcd626aaad3c766bb7dde7a--kpop-girls-nayoen-twice.jpg', 1, '2017-08-02 10:03:26'),
(3, 'Que wen momo', 'http://localhost:3000/images/18268660_1182063508586989_340124480382872343_n.jpg', 2, '2017-08-03 08:14:26'),
(4, '123123123', 'http://localhost:3000/images/matenme.JPG', 2, '2017-08-03 08:15:02'),
(5, 'asdjañsldjf', 'http://localhost:3000/images/', 1, '2017-08-03 08:19:01'),
(6, 'datasdadsfasd', 'http://localhost:3000/images/', 1, '2017-08-03 08:21:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `contrasena` varchar(20) NOT NULL,
  `fechaCreacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nombre`, `nick`, `imagen`, `contrasena`, `fechaCreacion`) VALUES
(1, 'Marco', 'marbal', 'http://localhost:3000/images/20294018_801292496698179_4452854416673281212_n.jpg', '1234', '2017-08-02 06:54:17'),
(2, 'Daniel', 'daniAr', NULL, '321', '2017-08-03 07:28:42');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idComentario`),
  ADD KEY `idPublicacion` (`idPublicacion`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`idPublicacion`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `idPublicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`idPublicacion`) REFERENCES `publicacion` (`idPublicacion`),
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD CONSTRAINT `publicacion_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
